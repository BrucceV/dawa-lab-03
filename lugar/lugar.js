const axios = require("axios");

const getLugarLatLng = async (dir) => {
    try{
        const encodeUrl = encodeURI(dir);

        const instance = axios.create({
            baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${encodeUrl}`,
            headers: {
                "x-rapidapi-key": "c7b3d08502msh8fc66522074db98p111c82jsn255acfeffaa4"}
        });

        const resp = await instance.get();

        /*if(resp.data.Results.length === 0){
            throw new Error(`No hay resultados para ${dir}`);
        }*/

        const data = resp.data.Results[0];
        const direccion = data.name;
        const lat = data.lat;
        const lng = data.lon;

        const encodeUrl2 = encodeURI(dir);

        const instance2 = axios.create({
            baseURL: `https://api.openweathermap.org/data/2.5/weather?q=${encodeUrl2}&appid=6786226461b2b3b5148fc7a5b5c41ad5`,
        });

        const resp2 = await instance2.get();
        const data2 = resp2.data;
        const main = data2.weather[0].main;
        const weather = data2.weather[0].description;

        return {direccion, lat, lng, weather, main};
    }catch (error){
        console.log(error)
    }
};

module.exports = {
    getLugarLatLng
}