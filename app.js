/*const axios = require('axios')*/

const lugar = require('./lugar/lugar')

const argv = require('yargs').options({
    direccion: {
        alias: 'd',
        desc: 'Direccion de la ciudad para obtener el clima',
        demand: true
    }
}).argv

lugar.getLugarLatLng(argv.direccion)
    .then(console.log)
/*
console.log(argv.direccion)

const encodeUrl = encodeURI(argv.direccion)
console.log(encodeUrl)

const instance = axios.create({
    baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${encodeUrl}`,
    headers: {'x-rapidapi-key': 'c7b3d08502msh8fc66522074db98p111c82jsn255acfeffaa4'}
});

instance.get()
    .then( resp => {
        console.log(resp.data.Results[0])
    })
    .catch( err => {
        console.log('ERRRRRORR!!!')
    })
    */